#!/usr/bin/env python3

import f90nml as f90
import os
# We can't test everything here without duplicating logic/code from GS2
# so just focus on main "silly" errors.

# Tests implemented
# * Does restart dir exist if reading/writing restart
# * Does response dir exist if reading/writing response

# Tests to add
# * Do restart files exist if reading restart
# * Do response files exist if reading response
# * Does equilibrium file exist if using numerical equilibrium type
# * Is avail_cpu_time set (Warning)
# * Missing index namelists (e.g. nspec = 2 but only one species list)?
# * Nonlinear but not saving restarts (Warning)

# Some of these conditions could be auto-fixed (e.g. writing restarts
# but directory doesn't exist).

# Currently tests returning logicals. Would be nice to return a test
# results case or similar so we can provide customised messages for later
# consumption etc. Possibly even a callback to fix the issue.

# Utilities
def get_value_or_default(nml, flag, namelist, default = None):
    if namelist in nml.keys():
        return nml[namelist].get(flag, default)
    else:
        return default

class GS2InputTest():
    def __init__(self):
        pass
    def test(self):
        raise NotImplemented
    def fix(self):
        raise NotImplemented

class GS2InputTestResult():
    def __init__(self, case, passed = True, message = 'Passed'):
        self.passed = passed
        self.message = message
        self.case = case

    def record_fail(self, message = 'Failed'):
        self.passed = False
        self.message = message

    def record_pass(self, message = 'Passed'):
        self.passed = True
        self.message = message

    def report(self):
        print(self.message)

    def __bool__(self):
        return self.passed

class RestartDirExists(GS2InputTest):
    def __init__(self, case):
        self.case = case

    def test(self):
        result = GS2InputTestResult(self.case)
        if (self.case._is_reading_restarts() or self.case._is_writing_restarts()):
            if not os.path.exists(self.case._get_restart_dir()):
                # Need to report/record error here
                result.record_fail('Error ({i}) : Wants to read/write to restart_dir "{r}" but does not exist'.format(i = self.case.filename, r = self.case._get_restart_dir()))
        return result

    def fix(self):
        if not self.test():
            os.path.makedirs(self.case._get_restart_dir())

class ResponseDirExists(GS2InputTest):
    def __init__(self, case):
        self.case = case

    def test(self):
        result = GS2InputTestResult(self.case)
        if (self.case._is_reading_response() or self.case._is_writing_response()):
            if not os.path.exists(self.case._get_response_dir()):
                # Need to report/record error here
                result.record_fail('Error ({i}) : Wants to read/write to response_dir "{r}" but does not exist'.format(i = self.case.filename, r = self.case._get_response_dir()))
        return result

    def fix(self):
        if not self.test():
            os.path.makedirs(self.case._get_response_dir())

class GS2Input():
    def __init__(self, filename, verbose = False):
        self.filename = filename
        self.nml = f90.read(self.filename)
        self.verbose = verbose

    def _get_restart_dir(self):
        namelist_name = 'init_g_knobs'
        default_value = './'
        flag_name = 'restart_dir'
        value = get_value_or_default(self.nml, flag_name, namelist_name, default_value)
        return os.path.abspath(os.path.join(os.path.dirname(self.filename), value))

    def _is_reading_restarts(self):
        #Shoud this be global?
        restarting_ginit_types = ['many', 'file']
        ginit_option = get_value_or_default(
            self.nml, 'ginit_option', 'init_g_knobs', default = 'default'
        )
        return ginit_option in restarting_ginit_types

    def _is_writing_restarts(self):
        save_for_restart = get_value_or_default(
            self.nml, 'save_for_restart', 'gs2_diagnostics_knobs', default = False
        )
        save_distfn = get_value_or_default(
            self.nml, 'save_distfn', 'gs2_diagnostics_knobs', default = False
        )
        return save_for_restart or save_distfn

    def _get_response_dir(self):
        namelist_name = 'fields_knobs'
        default_value = './'
        flag_name = 'response_dir'
        value = get_value_or_default(self.nml, flag_name, namelist_name, default_value)
        return os.path.abspath(os.path.join(os.path.dirname(self.filename), value))

    def _is_reading_response(self):
        read_response = get_value_or_default(
            self.nml, 'dump_response', 'fields_knobs', default = False
        )
        return read_response

    def _is_writing_response(self):
        write_response = get_value_or_default(
            self.nml, 'read_response', 'fields_knobs', default = False
        )
        return write_response

    def validate(self):
        # Create set of tests, expect in future may want to compose
        # the set of tests run based on flags etc.
        tests = [
            RestartDirExists,
            ResponseDirExists,
        ]

        test_results = [ x(self).test() for x in tests]
        valid = all(
            test_results
        )

        if not valid and self.verbose:
            _ = [x.report() for x in test_results if not x]
        return valid

if __name__=="__main__":
    import argparse
    import sys
    from glob import glob

    parser = argparse.ArgumentParser(
        description='Check a gs2 input file for common errors.')

    parser.add_argument("--input", action = "append",
                        help = "The input file to sanity check.",
                        required = True)

    parser.add_argument("--verbose", action = "store_true",
                        help = "If set be more verbose.",
                        required = False)

    # Perhaps we should add flags to enable/disable tests?
    args = parser.parse_args()

    # Create a flat list of all matching file types
    input_files_tmp = [[os.path.abspath(y) for y in glob(x)] for x in args.input]
    input_files = input_files_tmp[0]
    if len(input_files_tmp) > 1:
        _ = [input_files.extend(x) for x in input_files_tmp[1:]]

    # Make sure list only contains unique entries
    input_files = set(input_files)

    # Make sure we have at least one input file
    if len(input_files) < 1:
        raise Exception(
            "Couldn't find any input files matching {i}".format(i=args.input)
        )

    if args.verbose:
        print("Checking inputs : ",input_files)

    # Design decision here -- do we abort on the first error or do we
    # check everything and then report a summary of the errors encountered?
    # First is probably easier but second might be more helpful.

    inputs = [GS2Input(x, args.verbose) for x in input_files]
    result = dict([(x,x.validate()) for x in inputs])

    passed = result.values()

    if not all(passed):
        print('Failing tests')
        if args.verbose or len(inputs) > 1:
            _ = [print('\t{i} failed tests'.format(i=x[0].filename)) for x in result.items() if not x[1]]
        sys.exit(1)
    else:
        if args.verbose:
            print('All checks passed')
        sys.exit(0)
